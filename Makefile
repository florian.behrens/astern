LIBS = -D_GLIBCXX_USE_CXX11_ABI=0
FLAGS = -g -O0 -Wall -std=c++11 

run: main
	./main
.PHONY: run

debug: main
	gdb ./main
.PHONY: debug

clean:
	rm main 
.PHONY: clean

main: a_stern.cpp unit.h text_visualizer.h
	$(CXX) $(FLAGS) $(LIBS) -o $@ $^

