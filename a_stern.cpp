
#include "text_visualizer.h"
#include "unit.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <queue>
#include <sstream>
#include <string>
#include <list>

#define AT(h, w) ((h)*width + (w))

struct Coordinate {
	double x, y;
};

// Ein Graph, der Koordinaten von Knoten speichert.
class CoordinateGraph : public DistanceGraph {
private:
	std::vector<NeighborT> adj_list;
	std::vector<Coordinate> coords;
	double heuristic_multiplier = 1;
public:
	CoordinateGraph(const std::string& filename, bool is_maze=false) {
		if(is_maze){
			from_maze(filename);
		} else {
			from_coords(filename);
		}
	}

	CoordinateGraph(std::vector<CellType>& maze, size_t height, size_t width) {
		from_maze_vector(maze, height, width); 
	}

	void from_maze(const std::string& filename) {
		std::ifstream in (filename);
		size_t height, width;
		in >> height >> width;
		
		std::vector<CellType> maze;
		maze.assign(height*width, CellType::Ground); 

		for(size_t h = 0; h<height; h++){
			for(size_t w = 0; w<width; w++) {
				char c;
				in >> c;		
				if (c == '#') {
					maze[AT(h,w)] = CellType::Wall;
				}
			}
		}

		in.close();
		from_maze_vector(maze, height, width);
	}

	void from_maze_vector(std::vector<CellType>& maze, size_t height, size_t width) {
		this->vertexCount = height * width;
		for(size_t h = 0; h < height; h++) {
			for(size_t w = 0; w < width; w++) {
				VertexT cur = AT(h, w);
				NeighborT ns = *(new NeighborT);
				Coordinate c = *(new Coordinate);
				c.x = h;
				c.y = w;	
				this->coords.push_back(c);
				if(maze[cur] == CellType::Wall) {
					this->adj_list.push_back(ns);
					continue;
				}

				// oben
				if (h >= 1 && maze[AT(h-1,w)] != CellType::Wall) {
					LocalEdgeT e = *(new LocalEdgeT);
					e.first = AT(h-1, w);
					e.second = 1;
					ns.push_back(e);
				}
				// rechts
				if (width - w > 1 && maze[AT(h, w+1)] != CellType::Wall) {
					LocalEdgeT e = *(new LocalEdgeT);
					e.first = AT(h, w+1);
					e.second = 1;
					ns.push_back(e);
				}
				// unten
				if (h < height -1 && maze[AT(h+1, w)] != CellType::Wall) {
					LocalEdgeT e = *(new LocalEdgeT);
					e.first = AT(h+1, w);
					e.second = 1;
					ns.push_back(e);
				}
				// links
				if (w >= 1 && maze[AT(h, w-1)] != CellType::Wall) {
					LocalEdgeT e = *(new LocalEdgeT);
					e.first = AT(h, w-1);
					e.second = 1;
					ns.push_back(e);
				}

				this->adj_list.push_back(ns);
			}
		}
		if (this->adj_list.size() != this->vertexCount) {
			std::cerr << "Not enough adj lists!" << std::endl;
			exit(-1);
		}
	}
	
	void from_coords(const std::string& filename) {
		std::ifstream in (filename);		
		size_t N, M;
		in >> N >> M;		

		this->vertexCount = N;

		// Create an empty neighbors list for every vertex
		NeighborT emptyNeighbors;
		this->adj_list.assign(N, emptyNeighbors);
		for(size_t i = 0; i < M; i++) {
			VertexT from;
			LocalEdgeT *e = new LocalEdgeT;						
			in >> from >> e->first >> e->second;	
			this->adj_list[from].push_back(*e);	
		}

		for(VertexT v = 0; v < N; v++) {
			Coordinate *c = new Coordinate;
			in >> c->x >> c->y;	
			this->coords.push_back(*c);
		}
		in.close();
	}

	void set_heuristic_multiplier(double m) {
		this->heuristic_multiplier = m;
	}

    const NeighborT& getNeighbors( VertexT v) const override {
		if(v >= this->vertexCount) {
			std::cerr << "This vertex does not exist!" << std::endl;
			exit(-1);
		}
		return adj_list[v];
	}
    
    CostT estimatedCost( VertexT from, VertexT to) const override {
		if(from >= this->vertexCount || to >= this->vertexCount) {
			std::cerr << "This vertex does not exist!" << std::endl;
			exit(-1);
		}
		double c = this->cost(from, to);	
		if(c < infty) {
			return c;
		}
		Coordinate a = this->coords[from];
		Coordinate b = this->coords[to];
		return sqrt( (a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y));
	}
    
    CostT cost( VertexT from, VertexT to) const override {
		NeighborT adj =	this->getNeighbors(from); 	
		if(from == to) {
			return 0;
		}
		for(size_t i = 0; i < adj.size(); i++) {
			LocalEdgeT e = adj[i];	
			if(e.first == to) {
				return e.second;
			}
		}
		return infty;
	}
};


struct DPair {
	VertexT v;
	CostT cost;
};

bool operator<(DPair& a, DPair& b){
	//inverse definition, as pop_heap drops greatest according to operator
	return a.cost > b.cost;
}

void Dijkstra(const DistanceGraph& g, GraphVisualizer& v, VertexT start, std::vector<CostT>& distances) {
	distances.assign(g.numVertices(), infty);
	distances[start] = 0;
	
	std::vector<DPair> q;
	for(size_t i=0;i<g.numVertices();i++){
		if(i == start)	continue;
		DPair p;
		p.v = i;
		p.cost = g.cost(start, i);
		q.push_back(p);
	}

	std::make_heap(q.begin(), q.end());

	while(q.size() != 0) {
		std::pop_heap(q.begin(), q.end());
		DPair w = q.back();
		q.pop_back();
		distances[w.v] = w.cost;

		for(size_t i=0;i<q.size();i++) {
			CostT d1 = q[i].cost;
			CostT d2 = w.cost + g.cost(w.v, q[i].v);			
			q[i].cost = d1 > d2 ? d2 : d1; 
		}
		std::make_heap(q.begin(), q.end());
	}
}

struct APair {
	VertexT v;
	CostT cost;
};

class APairCompare {
	private:
		DistanceGraph &g;
		VertexT target;
	public: 
		APairCompare(DistanceGraph& g, VertexT target):g(g),target(target) {}

		bool operator() (APair& a, APair& b){
			CostT c1 = a.cost + g.estimatedCost(a.v, target);
			CostT c2 = b.cost + g.estimatedCost(b.v, target);

			return c1 > c2;
		}
};


bool A_star(DistanceGraph& g, GraphVisualizer& v, VertexT start, VertexT ziel, std::list<VertexT>& weg) {
	std::vector<VertexT> predecessor;
	predecessor.assign(g.numVertices(), g.numVertices());

	std::vector<APair> open;
	std::vector<APair> closed;

	APair p;
	p.v = start;
	p.cost = 0;
	open.push_back(p);

	APairCompare cmp = APairCompare(g, ziel);

	while(open.size() != 0){
		std::make_heap(open.begin(), open.end(), cmp);
		std::pop_heap(open.begin(), open.end(), cmp);
		APair w = open.back();
		open.pop_back();

		if(w.v == ziel) {
			VertexT cur = w.v;
			while(cur != start) {
				weg.insert(weg.begin(), cur);
				cur = predecessor[cur];
			}
			weg.insert(weg.begin(), start);
			return true;
		}
		
		closed.push_back(w);
		// Expand node
		const DistanceGraph::NeighborT ns = g.getNeighbors(w.v); 
		for(size_t n = 0; n < ns.size(); n++) {
			DistanceGraph::LocalEdgeT edge = ns[n];
			bool in_closed_list = false;
			for(size_t j = 0; j < closed.size(); j++) {
				if(closed[j].v == edge.first) {
					in_closed_list = true;
					break;
				}
			}
			if(in_closed_list) {
				continue;
			}

			CostT tentative_g = w.cost + g.cost(w.v, edge.first);
			bool successor_open = false;
			APair succ;
			for(size_t j = 0; j < open.size(); j++) {
				if(open[j].v == edge.first) {
					successor_open = true;
					succ = open[j];
					break;
				}
			}
			if(successor_open && tentative_g >= succ.cost) {
				continue;
			}
			predecessor[edge.first] = w.v;
			if(!successor_open) {
				APair s = *(new APair);
				s.v = edge.first;
				s.cost = tentative_g;
				open.push_back(s);
			} else {
				succ.cost = tentative_g;
			}
		}	
	
	}

    return false; // Kein Weg gefunden.
}

void test_dijkstra_coordinate_graphs() {
	for(size_t t=1; t<5;t++){
		std::stringstream ss;
		ss << "daten/Graph" << t << ".dat";
		CoordinateGraph g(ss.str());
		TextVisualizer v;
		std::vector<CostT> distances;
		for(size_t i=0;i<g.numVertices();i++){
			Dijkstra(g, v, i, distances);
			PruefeDijkstra(t, i, distances);
		}
	}
}

void test_astar_coordinate_graphs() {
	for(size_t t=1; t<6;t++){
		std::stringstream ss;
		ss << "daten/Maze" << t << ".dat";
		CoordinateGraph g(ss.str(), true);
		TextVisualizer v;
		
		std::cout << "Graph has " << g.numVertices() << " vertices" << std::endl;
		for(VertexT start = 0; start < g.numVertices(); start++) {
			for(VertexT end = start + 1; end < g.numVertices(); end++) {
				std::list<VertexT> weg;
				bool path_exists = A_star(g, v, start, end, weg);
				if (path_exists) {
					PruefeWeg(t+4, weg); 
				}
			}
		}

	}
}


void test_coordinate(int nBsp) {
    // PruefeHeuristik
	std::stringstream ss;
	ss << "daten/Graph" << nBsp << ".dat";
	CoordinateGraph g(ss.str(), false);
	TextVisualizer v;

	// Aachen - München Luftlinie: 493,59km
	// Aachen: 50.7753 6.0839
	// München: 48.1351 11.5820
	// Euklidische Distanz: (50.7753 - 48.1351)^2 + (6.0839 - 11.5820)^2 = 37.20
	// 37.20 * x = 493.59 -> x = 493.59 / 37.2 = 13.2685
	// Damit wir uns sicher unterschätzen, nehmen wir einen Faktor 12
	
	if(nBsp == 3) {
		g.set_heuristic_multiplier(12);
	}

	// Indem wir mit 12 multiplizieren, erhalten wir einen Unterschätzer für die
	// Kilometer. Jetzt schätzen wir aus der Anzahl der Kilometer die 
	// Fahrtzeit ab. Nach Wikipedia beträgt die durchschnittliche
	// Überlandgeschwindigkeit knapp 60 kmh, was wir mal probieren.
	// -> h * 12 / 60 * 60
	if(nBsp == 4) {
		g.set_heuristic_multiplier(12 / 60 * 60);
	}
	
	PruefeHeuristik(g);
	
	std::vector<CostT> distances;
	for(size_t i=0;i<g.numVertices();i++){
		Dijkstra(g, v, i, distances);
		PruefeDijkstra(nBsp, i, distances);
	}

	for(VertexT start = 0; start < g.numVertices(); start++) {
		for(VertexT end = 0; end < g.numVertices(); end++) {
			if(start == end) {
				continue;
			}
			std::list<VertexT> weg;
			std::cout << "Suche weg von " << start << " nach " << end << ".." << std::endl;
			bool path_exists = A_star(g, v, start, end, weg);
			if (path_exists) {
				PruefeWeg(nBsp, weg); 
			} else {
				std::cout << "kein Weg gefunden!" << std::endl;
			}
			std::cout << std::endl << std::endl;
		}
	}

}

void test_file_maze(int nBsp) {
	std::stringstream ss;
	ss << "daten/Maze" << (nBsp - 4) << ".dat";
	CoordinateGraph g(ss.str(), true);
	TextVisualizer v;
	
	PruefeHeuristik(g);

	auto start_end_pairs = StartZielPaare(nBsp);
	std::cout << "There are " << start_end_pairs.size() << " tests to be made." << std::endl;
	for(auto pair : start_end_pairs) {
		std::list<VertexT> weg;
		bool path_exists = A_star(g, v, pair.first, pair.second, weg);
		if(path_exists){
			PruefeWeg(nBsp, weg);
		} else {
			std::cout << "No hay path" << std::endl;
		}
	}
}

void test_gen_maze() {
	size_t height = 256, width = 256;
	std::vector<CellType> maze = ErzeugeLabyrinth(height, width, 1337); 
	VertexT start, end;
	for(size_t h = 0; h < height; h++) {
		for(size_t w = 0; w < width; w++) {
			if(maze[AT(h, w)] == CellType::Start) {
				start = AT(h, w);
				maze[AT(h, w)] = CellType::Ground;
			}
			if(maze[AT(h,w)] == CellType::Destination) {
				end = AT(h, w);
				maze[AT(h, w)] = CellType::Ground;
			}
		}
	}

	CoordinateGraph g (maze, height, width);
	TextVisualizer v;

	//PruefeHeuristik(g);

	std::list<VertexT> weg;
	bool path_exists = A_star(g, v, start, end, weg);
	if(path_exists){
		PruefeWeg(10, weg);
	}
}

int main()
{
    // Frage Beispielnummer vom User ab
    std::cout << "Bruder, gib Beispielnummer! -> ";
	int nBsp;
	std::cin >> nBsp;
	if(nBsp < 1 || nBsp > 10) {
		std::cout << "Bruder, Beispiele sind 1 bis 10" << std::endl;
		exit(-1);
	}
    // Lade die zugehoerige Textdatei in einen Graphen
	if(nBsp >= 1 && nBsp <= 4) {
		test_coordinate(nBsp);
	} else if (nBsp >= 5 && nBsp <= 9) {
		test_file_maze(nBsp);
	} else {
		test_gen_maze();
	}
		
   	return 0;
}
 

